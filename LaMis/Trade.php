<?php
// +----------------------------------------------------------------------
// | Lakala
// +----------------------------------------------------------------------
// | 版权所有 2023~2023 宝龙 [ 117384790@qq.com ]
// +----------------------------------------------------------------------
// | 官方网站: https://www.7dcs.com
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org ) 
// +----------------------------------------------------------------------
namespace LaMis;

use Lib\Contracts\BasicMis;
use Lib\Exceptions\InvalidInstanceException;
use Lib\Exceptions\InvalidResponseException;

class Trade extends BasicMis {

    /**
     * 推送数据
     * @param $options
     * @return mixed
     * @throws \Lib\Exceptions\InvalidResponseException
     * @throws \Lib\Exceptions\LocalCacheException
     */
    public function push($options){
        $url = 'open/v1/trade/push';
        $options['mch_id'] = $this->config->get('mch_id');
        $options['notify_url'] = $this->config->get('notify_url');
        return $this->callPostApi($url , $options, true);
    }

    /**
     * 查询数据
     * @return mixed
     * @throws \Lib\Exceptions\InvalidResponseException
     * @throws \Lib\Exceptions\LocalCacheException
     */
    public function find($options){
        $url = 'open/v1/trade/find';
        $options['mch_id'] = $this->config->get('mch_id');
        $options['notify_url'] = $this->config->get('notify_url');
        return $this->callPostApi($url , $options, true);
    }

    /**
     * 异步信息
     * @return mixed
     * @throws InvalidInstanceException
     */
    public function notify(){
        $data = json_decode(file_get_contents('php://input'),true);
        if (isset($data['sign']) && $this->getNotifySign($data)) {
            if ($data['header']['code'] === '00000'){
                return $data['body'];
            }else{
                throw new InvalidResponseException(
                    "Error: " .
                    (empty($result['header']['code']) ? '' : "{$result['header']['msg']} [{$result['header']['code']}]\r\n") .
                    $result['header']['code'], $result
                );
            }
        }
        throw new InvalidInstanceException('Invalid Notify.', '0');
    }
}