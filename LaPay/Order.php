<?php

// +----------------------------------------------------------------------
// | WeChatDeveloper
// +----------------------------------------------------------------------
// | 版权所有 2014~2023 ThinkAdmin [ thinkadmin.top ]
// +----------------------------------------------------------------------
// | 官方网站: https://thinkadmin.top
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// | 免责声明 ( https://thinkadmin.top/disclaimer )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/zoujingli/WeChatDeveloper
// | github 代码仓库：https://github.com/zoujingli/WeChatDeveloper
// +----------------------------------------------------------------------

namespace LaPay;

use Lib\Contracts\BasicPay;
use Lib\Contracts\Tools;

/**
 * 微信商户订单
 * Class Order
 * @package WePay
 */
class Order extends BasicPay
{

    /**
     * 创建
     * @param array $options
     * @return mixed
     * @throws \Lib\Exceptions\InvalidResponseException
     * @throws \Lib\Exceptions\LocalCacheException
     */
    public function create(array $options)
    {
        $url = 'v3/ccss/counter/order/create';
        return $this->callPostApi($url , $options, true);
    }


    public function query(){}
}