<?php

// +----------------------------------------------------------------------
// | WeChatDeveloper
// +----------------------------------------------------------------------
// | 版权所有 2014~2023 ThinkAdmin [ thinkadmin.top ]
// +----------------------------------------------------------------------
// | 官方网站: https://thinkadmin.top
// +----------------------------------------------------------------------
// | 开源协议 ( https://mit-license.org )
// | 免责声明 ( https://thinkadmin.top/disclaimer )
// +----------------------------------------------------------------------
// | gitee 代码仓库：https://gitee.com/zoujingli/WeChatDeveloper
// | github 代码仓库：https://github.com/zoujingli/WeChatDeveloper
// +----------------------------------------------------------------------

namespace LaPay;

use Lib\Contracts\BasicPay;
use Lib\Contracts\Tools;
use Lib\Exceptions\InvalidInstanceException;
use Lib\Exceptions\InvalidResponseException;
use think\admin\Exception;
use think\admin\Library;

/**
 * 微信商户订单
 * Class Order
 * @package WePay
 */
class Refund extends BasicPay
{

    /**
     * 创建
     * @param array $options
     * @return mixed
     * @throws \Lib\Exceptions\InvalidResponseException
     * @throws \Lib\Exceptions\LocalCacheException
     */
    public function create(array $options)
    {
        $url = 'v3/rfd/refund_front/refund';
        $options['merchant_no'] = $this->config->get('mch_id');
        $options['notify_url'] = $this->config->get('notify_url').'?attach='.$options['attach']??'callback';
        $options['term_no'] = $this->config->get('term_no');
        $options['location_info'] = ['request_ip' => ( Library::$sapp->request->ip() ?: '127.0.0.1' ) ,'baseStation'=>'','location' => ''];
        if (isset($options['attach'])) unset($options['attach']);
        return $this->callPostApi($url , $options, true);
    }

    /**
     * 退单查询
     * @param array $options
     * @return void
     */
    public function query(array $options){
        
    }

    /**
     * 退款效验
     * @return mixed
     * @throws Exception
     */
    public function notify(){
        $body = file_get_contents('php://input');
        $Authorization = Library::$sapp->request->header('Authorization');
        $attach = Library::$sapp->request->param('attach','');
        $pattern = '/timestamp="(\d+)",nonce_str="(\w+)",signature="([^"]+)"/';
        preg_match($pattern, $Authorization, $matches);
        if (count($matches) == 0)  throw new Exception('Authorization Notify.', '0');

        $timestamp = $matches[1];
        $nonce_str = $matches[2];
        $signature = $matches[3];

        $verifyData = $timestamp. "\n" . $nonce_str . "\n" . $body . "\n";
        $key = openssl_pkey_get_public($this->config['platform_public_key']);

        if (openssl_verify($verifyData, base64_decode($signature), $key, OPENSSL_ALGO_SHA256) === 1){
            $data = json_decode($body,true);
            if ($data['trade_status'] === 'SUCCESS'){
                $data['attach'] = $attach;
                return $data;
            }else{
                throw new Exception(
                    "返回数据状态异常",$data
                );
            }
        }else{
            throw new Exception('签名效验失败', '0');
        }


    }
}
